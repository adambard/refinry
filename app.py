from flask import Flask, render_template, request, flash, jsonify
from refinry.mail import POST_MESSAGE, send_mail

app = Flask(__name__)
application = app
app.secret_key = 'asdfplk20h230hds0fajsd90fja9j32w9f0aksddsf akdskkadsfk9kd'

###################################
# Views

def page(name):
    ajax = request.args.get('ajax', False)
    if ajax:
        return jsonify({'html': render_template('_{0}.html'.format(name))})

    return render_template('{0}.html'.format(name))

@app.route('/')
def index():
    return page('index')

@app.route('/about/')
def about():
    return page('about')

@app.route('/contact/', methods=['GET', 'POST'])
def contact():
    if request.method == "get":
        return page('contact')
    else:
        # Send the contents of request.form
        message = POST_MESSAGE
        message += '\n'.join(['{0}: {1}'.format(k, request.form[k]) for k in request.form])
        if send_mail(message, from_email=request.form.get('email', 'noreply@refinry.com')):
            return jsonify({'status': 'success', 'message': 'Thanks for your interest. Please expect a reply within 1 business day (but probably sooner).'})
        else:
            return jsonify({'status': 'error', 'message': 'A problem occurred. This should never happen. Forget you saw anything.'})


##################################3
# Routing
(lambda: page('index'))
app.route('/about/')(lambda: page('about'))
app.route('/contact/')(lambda: page('contact'))


if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000, debug=True)

