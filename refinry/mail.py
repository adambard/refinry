from email.mime.text import MIMEText
import smtplib

def send_mail(message, from_email='noreply@refinry.com', subject='New Message From refinry.com'):
    msg = MIMEText(message)
    msg['From'] = str(from_email)
    msg['To'] = 'adam@adambard.com'
    msg['Subject'] = '[Refinry.com] {0}'.format(subject)

    try:
        s = smtplib.SMTP('localhost')
        s.sendmail(from_email, 'adam@adambard.com', msg.as_string())
        s.quit()
        return True
    except Exception as e:
        return False

POST_MESSAGE = """
A new contact has come in from refinry.com.

Data:
"""
