$ ->
    window.base_path = window.location.pathname

    handle_ajax_link = (href) ->
        href = href.replace(/.*#!/, '')
        href = href + "?ajax=1"
        $.ajax
            url: href
            type: 'get'
            dataType: 'json'
            success: (res) ->
                el = $('#body')
                el.fadeOut(500, ->
                    el.html(res.html)
                    el.fadeIn(500))
            error: ->
                alert('error')

    $('a').each ->
        link = $(this).attr('href')
        if link.match /\/.*/
            $(this).attr('href', "/#!#{link}")
            $(this).click(->
                el = $(this)
                href = el.attr('href')
                handle_ajax_link(href))

    console.log '#!' + window.base_path
    if window.location.hash and window.location.hash != '#!' + window.base_path
        handle_ajax_link(window.location.hash)

