(function() {

  $(function() {
    var handle_ajax_link;
    window.base_path = window.location.pathname;
    handle_ajax_link = function(href) {
      href = href.replace(/.*#!/, '');
      href = href + "?ajax=1";
      return $.ajax({
        url: href,
        type: 'get',
        dataType: 'json',
        success: function(res) {
          var el;
          el = $('#body');
          return el.fadeOut(500, function() {
            el.html(res.html);
            return el.fadeIn(500);
          });
        },
        error: function() {
          return alert('error');
        }
      });
    };
    $('a').each(function() {
      var link;
      link = $(this).attr('href');
      if (link.match(/\/.*/)) {
        $(this).attr('href', "/#!" + link);
        return $(this).click(function() {
          var el, href;
          el = $(this);
          href = el.attr('href');
          return handle_ajax_link(href);
        });
      }
    });
    console.log('#!' + window.base_path);
    if (window.location.hash && window.location.hash !== '#!' + window.base_path) {
      return handle_ajax_link(window.location.hash);
    }
  });

}).call(this);
